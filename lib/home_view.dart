import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class HomeView extends StatefulWidget {
  const HomeView({super.key});

  @override
  State<StatefulWidget> createState() => HomeViewState();
}

class HomeViewState extends State<HomeView> {
  final TextEditingController _amountController = TextEditingController();
  num _percent = 0.10;
  num _tip = 0;
  num _amount = 0;
  num _amountWithTip = 0;

  @override
  void initState() {
    super.initState();
    _amountController.text = '';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF232A32),
      appBar: AppBar(
        backgroundColor: Color(0xFF343C49),
        centerTitle: false,
        title: Text(
          'Tip Calculator',
          style: TextStyle(
            color: Colors.white,
            fontFamily: 'Exo2',
            fontSize: 24,
            fontWeight: FontWeight.bold
          ),
        ),
      ),
      body: Padding(
        padding: EdgeInsets.all(20),
        child: Column(
          children: [
            Expanded(
              child: Container(
                width: double.maxFinite,
                padding: EdgeInsets.all(20),
                decoration: BoxDecoration(
                  color: Color(0xFF343C49),
                  borderRadius: BorderRadius.circular(10)
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Сумма',
                          style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'Exo2',
                            fontSize: 20,
                          ),
                        ),
                        SizedBox(
                          width: 180,
                          height: 45,
                          child: TextField(
                            controller: _amountController,
                            onChanged: (value) {
                              setState(() {
                                _amount = value.isNotEmpty ? num.parse(value) : 0;
                                _tip = num.parse((_amount * _percent).toStringAsFixed(2));
                                _amountWithTip = num.parse((_amount + _tip).toStringAsFixed(2));
                              });
                            },
                            textAlign: TextAlign.center,
                            textAlignVertical: TextAlignVertical.center,
                            cursorColor: Color(0xFF59DAA4),
                            style: TextStyle(
                              color: Colors.white,
                              fontFamily: 'Exo2',
                            ),
                            keyboardType: TextInputType.numberWithOptions(decimal: true),
                            inputFormatters: [
                              FilteringTextInputFormatter.deny(',', replacementString: '.'),
                              FilteringTextInputFormatter.allow(RegExp(r'^\d{1,6}(\.\d{0,2})?')),
                            ],
                            decoration: InputDecoration(
                              icon: Icon(Icons.currency_ruble_rounded, color: Color(0xFF59DAA4)),
                              contentPadding: EdgeInsets.all(10),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(width: 1, color: Color(0xFF59DAA4)),
                                borderRadius: BorderRadius.circular(50.0),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(width: 1, color: Color(0xFF59DAA4)),
                                borderRadius: BorderRadius.circular(50.0),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Чаевые',
                          style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'Exo2',
                            fontSize: 20,
                          ),
                        ),
                        SizedBox(
                          width: 180,
                          height: 45,
                          child: SegmentedButton<num>(
                            showSelectedIcon: false,
                            style: SegmentedButton.styleFrom(
                              foregroundColor: Color(0xFF59DAA4),
                              selectedBackgroundColor: Color(0xFF59DAA4),
                              selectedForegroundColor: Color(0xFF343C49),
                              side: BorderSide(color: Color(0xFF59DAA4)),
                            ),
                            segments: const <ButtonSegment<num>> [
                              ButtonSegment<num>(
                                value: 0.10,
                                label: Text('10%'),
                              ),
                              ButtonSegment<num>(
                                value: 0.15,
                                label: Text('15%'),
                              ),
                              ButtonSegment<num>(
                                value: 0.20,
                                label: Text('20%'),
                              ),
                            ],
                            selected: <num>{_percent},
                            onSelectionChanged: (value) {
                              setState(() {
                                _percent = value.first;
                                _amount = _amountController.text.isNotEmpty ? num.parse(_amountController.text) : 0;
                                _tip = num.parse((_amount * _percent).toStringAsFixed(2));
                                _amountWithTip = num.parse((_amount + _tip).toStringAsFixed(2));
                              });
                            },
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(height: 20),
            Expanded(
              child: Container(
                width: double.maxFinite,
                padding: EdgeInsets.all(20),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  border: Border.all(color: Color(0xFF59DAA4)),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Чаевые',
                          style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'Exo2',
                            fontSize: 20,
                          ),
                        ),
                        Text(
                          _tip.toString(),
                          style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'Exo2',
                            fontSize: 20,
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Всего',
                          style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'Exo2',
                            fontSize: 20,
                          ),
                        ),
                        Text(
                          _amountWithTip.toString(),
                          style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'Exo2',
                            fontSize: 20,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(height: 20),
            SizedBox(
              width: double.maxFinite,
              child: MaterialButton(
                onPressed: () {
                  setState(() {
                    _percent = 0.10;
                    _tip = 0;
                    _amount = 0;
                    _amountWithTip = 0;
                    _amountController.text = '';
                  });
                },
                height: 60,
                color: Color(0xFF343C49),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Text(
                  'Очистить',
                  style: TextStyle(
                    color: Colors.white,
                    fontFamily: 'Exo2',
                    fontSize: 20,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}