import 'package:flutter/material.dart';
import 'package:tip_calculator/home_view.dart';

class TipCalculatorApp extends StatelessWidget {
  const TipCalculatorApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Tip Calculator',
      home: HomeView(),
    );
  }
}
